import React from 'react';
import { Alert, Linking, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default class PrivacyScreen extends React.Component {
    static route = {
        navigationBar: {
          title: "Gizlilik Sözleşmesi",
          titleStyle: {
            fontWeight: "bold"
          },
          tintColor: "#FF3366"
        }
      };

    constructor(props) {
        super(props)
        this.state = {
            content: null
        };
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, paddingTop: 20, backgroundColor: '#e4e3eb' }}>
                        <Text style={styles.contentTitle}>1. Kapsanan Hizmetler</Text>
                        <Text style={styles.content}><TouchableOpacity onPress={()=> Linking.openURL('http://buluruz.biz')}><Text style={{fontWeight: 'bold', color:'blue'}}>buluruz.biz</Text></TouchableOpacity> web sayfası ve mobil uygulaması kullanılan hizmetleri kapsamaktadır.</Text>
                    <Text style={styles.contentTitle}>2. Toplanan Veriler</Text>
                    <Text style={styles.content}>Gizlilik sözleşmesinde belirtilen şekilde kişisel verilerinizi topluyoruz. Bu bilgiler adınız, e-posta adresiniz, fotoğrfınızı içerebilir. Ayrıca kişisel verilerinizi kamuya açık alanlardan ve sosyal medya platformlarından temin edebiliriz. Kişisel verilerinizi
        siz ve diğer üyeler ile bilgi alışverişi amacıyla kullanmaktayız. Kişisel verilerinizi tarafınızca kullanımı gerekli olmadıkça ve izniniz olmadan üçüncü kişilerle paylaşmamaktayız.</Text>
                    <Text style={styles.content}>Kişisel verileriniz alınmadan uygulama ve web sitemizi kullanmanız mümkündür.</Text>
                    <Text style={styles.contentTitle}>3. Üyelik ve Kayıtlar</Text>
                    <Text style={styles.content}>Belli hizmetlerin kullanılabilmesi için sosyal medya hesabınızdan kayıt olmanız gerekmektedir. Eğer bir sosyal medya hesabı ile kayıt olursanız, ilgili sosyal medya ağındaki gizlilik ayarlarınıza bağlı olarak bize belli bilgilere erişim ve kullanma hakkı
        tanımaktasınız.</Text>
                    <Text style={styles.content}>Profil fotoğrafınız, isminiz, soy isminiz, kullanıcı adınız ve e-posta adresiniz gibi bilgilerinizi, söz konusu bilgilerin Uygulama ve Web Sayfası üzerinden erişilebilir kılınması amacıyla sizden talep edebiliriz.</Text>

                    <Text style={styles.contentTitle}>4. Kullanım Kuralları</Text>
                    <Text style={styles.content}>
                        Web Sitesi ve Uygulama ile bunların içeriklerini ve hizmetlerini kanunlar, ahlak kuralları ve kamu düzenine uygun ve Şartlar ve Koşullara bağlı kalarak kullanacağınızı kabul ediyorsunuz. Ayrıca hizmetlerin ve/veya içeriklerin düzgün kullanımından
        yükümlüsünüz ve bunları, yasa dışı faaliyetler gerçekleştirmek veya üçüncü tarafların haklarını ihlal edecek şekilde suç oluşturmak ve/veya fikri ve sınai mülkiyet düzenlemesini ve uygulanabilir diğer tüm kanuni hükümleri ihlal etmek üzere kullanmamakla
        yükümlüsünüz.
                        </Text>
                    <Text style={styles.content}>
                        Web Sitesi veya Uygulama aracılığı ile diğer Hizmet Kullanıcıları ile kurmuş olduğunuz iletişimden ve yüklemiş olduğunuz, gönderdiğiniz veya ilettiğiniz tüm içerikten sadece siz sorumlusunuz. Ayrıca üçüncü taraflar tarafından yüklenmiş olan içerik
        haklarını çiğnemeyeceğinizi de taahhüt ediyorsunuz.
                        </Text>

                    <Text style={styles.content}>
                        Ek olarak, Buluruz Biz’in hiçbir koşulda diğer kullanıcıları yönetmeyeceğini de kabul ediyor ve onaylıyorsunuz.
                        </Text>

                    <Text style={styles.content}>
                        Ek olarak, Buluruz Biz’in hiçbir koşulda diğer kullanıcıları yönetmeyeceğini de kabul ediyor ve onaylıyorsunuz.
                        </Text>

                    <Text style={styles.content}>
                        Buluruz Biz, kendi açık yazılı izni olmaksızın otomatikleştirilmiş yazılım ve Gönderi Araçlarının doğrudan veya dolayı olarak kullanımını yasaklamaktadır. İlaveten, başkalarının adına Gönderi Araçları ile içerik göndermek Buluruz Biz’in yazılı izni
       veya lisansı olmadan yasaklanmıştır. Burada kullanıldığı şekliyle “Gönderi Aracı” terimi başkalarının adına İçerik gönderme sağlayan bir üçüncü taraf acente, hizmet veya aracı anlamına gelmektedir.
                        </Text>

                    <Text style={styles.content}>
                        Yasalara, ahlak kurallarına, kamu düzenine ve buradaki Şartlar ve Koşullara aykırı olacak her türlü malzeme ve bilgileri (veri, içerik, mesaj, çizim, ses dosyaları, resim, fotoğraflar, yazılım v.b.) iletmemeyi, yaymamayı, vermemeyi ve üçüncü taraflara
       sunmamayı kabul ediyorsunuz. Aşağıdakileri kabul ediyorsunuz:
                        </Text>

                    <Text style={styles.content}>
                        I.- Irkçı, yabancı düşmanı, pornografik veya insan haklarına karşı düşünülebilinecek olan içerik veya propagandayı girmeyeceğinizi ve yaymayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        II.- Erişim sağlayıcılara, onun tedarikçilerine veya üçüncü taraf internet kullanıcılarının bilgisayar sistemlerine zarar verebilecek olan ağ veri programlarını kullanamayacağınızı ve yaymayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        III.- Uluslararası anlaşmalar ile tanınan Temel haklar ve insan haklarını ihlal edecek olan herhangi bir bilgi, unsur veya içeriği yaymayacağınızı, iletmeyeceğinizi ve üçüncü taraflara sunmayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        IV.- Yasa dışı veya adil olmayan reklam durumu oluşturan herhangi bir bilgi, unsur veya içeriği yaymayacağınızı, iletmeyeceğinizi ve üçüncü taraflara sunmayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        V.- Bilgi alıcı kişileri yanlış yönlendirecek olan herhangi bir yanlış, doğru olmayan veya şüpheli içeriği yaymayacağınızı ve iletmeyeceğinizi.
                        </Text>

                    <Text style={styles.content}>
                        VI.- Buluruz Biz veya üçüncü taraflara fikri ve sınai mülkiyet hakkını, patentleri, ticari markaları veya telif haklarını ihlal edecek herhangi bir bilgi, unsur veya içeriği yaymayacağınızı, iletmeyeceğinizi veya sunmayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        VII.- Buluruz Biz veri tabanınını, taramama, dışarı aktarmama, toplamama, depolamama veya bu veritabanına erişmemeyi ya da Buluruz Biz reklam ve kullanıcı veri tabanının tümü ya da bir kısmını toplamayacağınızı.
                        </Text>

                    <Text style={styles.content}>
                        Buluruz Biz’i bu dokümanda belirtilen kuralların herhangi birisini ihlal etmeniz sonucu oluşan muhtemel tüm hak taleplerinden, para cezalarından, yaptırımlardan veya cezalardan dolayı zarar görmesini engelleyeceğinizi, Buluruz Biz’in zararlar için
        tazminat talep etme hakkını saklı tutarak kabul ediyorsunuz.
                        </Text>

                    <Text style={styles.content}>
                        Buluruz Biz, Web Sitesi ve Uygulamada gösterilen veri ve diğer içeriklerin kontrolü için en yüksek düzeyde çaba göstermektedir. Buna rağmen bir hata veya eksiklik belirlerseniz, bize info@buluruz.biz adresinden bildirmenizi rica ederiz. Buluruz Biz
       en kısa sürede size yanıt verecektir.
                        </Text>

                    <Text style={styles.contentTitle}>5. Uyarı ve kaldırma</Text>
                    <Text style={styles.content}>
                        Eğer telif hakkı ile korunan bir işinizin Buluruz Biz’de sizin onayınız olmadan yayınlandığını veya kişisel haklarınızın diğer kullanıcılar veya üçüncü taraflarca ihlal edildiğine inanıyorsanız bir ihlal bildirimi gönderebilirsiniz. Bu talepler sadece
        telif hakkı veya kişisel hak sahibi tarafından veya hak sahibinin yetkili kıldığı bir acente tarafından gerçekleştirilebilir. İhlal edildiği öne sürülen telif hakkı ihlali bildiriminin en hızlı ve kolay yolu, web formumuzdur: info@buluruz.biz
                        </Text>
                    </View>
                </ScrollView>
            </SafeAreaView >
        );
    }

    createAd = async () => {
        try {
            if (!this.state.content) {
                return Alert.alert(
                    'Talep Oluştur',
                    'Lütfen talebinize bi açıklama girin!',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: true }
                );
            }

            await fetch(`http://buluruz.biz/api/ads/create`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ content: this.state.content, shop: 'buy' })
            });

            Alert.alert(
                'Talep Oluştur',
                'Talebiniz başarıyla oluşturuldu. Onaylandıktan sonra yayınlanacaktır.',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: true }
            )
        } catch (error) {
            console.log(error);
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentTitle: {
        color: '#333',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold'
    },
    content: {
        color: '#333',
        textAlign: 'center',
        margin: 10
    }
})