import * as _ from "lodash";
import qs from "qs";
import React from "react";
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { SearchBar } from "react-native-elements";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Router } from "../../Router";

export default class HomeScreen extends React.Component {
  static route = {
    navigationBar: {
      title: "Buluruz.Biz",
      titleStyle: {
        fontSize: 21,
        fontWeight: "bold"
      },
      tintColor: "#FF3366",
      renderRight: () => (
        <TouchableOpacity
          style={{ alignSelf: "center", marginRight: 15, marginTop: 7 }}
        >
          <MaterialCommunityIcons
            name={"tune"}
            size={26}
            style={{ color: "#FF3366" }}
          />
        </TouchableOpacity>
      )
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ads: [],
      limit: 10,
      offset: 0,
      error: null,
      refreshing: false,
      searched: false
    };
  }

  componentDidMount() {
    this.getAds();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1, backgroundColor: "#e4e3eb" }}>
          {this.renderList()}
        </View>
      </SafeAreaView>
    );
  }

  renderList() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <FlatList
        keyExtractor={(item, index) => item._id}
        ListHeaderComponent={
          <SearchBar
            onChangeText={this.search}
            onClearText={() => {}}
            lightTheme={true}
            placeholder="Ara..."
          />
        }
        ItemSeparatorComponent={() => <View style={styles.listItemSeperator} />}
        onEndReached={this.loadMoreAds}
        data={this.state.ads}
        renderItem={({ item }) => this.renderListItem(item)}
      />
    );
  }

  getAds = async () => {
    try {
      const { limit, offset } = this.state;
      const query = qs.stringify({
        status: "active",
        shop: "buy",
        sort: "-createdAt",
        limit,
        offset
      });
      const url = `http://buluruz.biz/api/ads?${query}`;
      await this.setState({ loading: true });
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      });
      const ads = await response.json();
      this.setState({
        ads: [...this.state.ads, ...ads],
        error: response.error || null,
        loading: false,
        refreshing: false
      });
    } catch (error) {
      console.error(error);
      this.setState({ error, loading: false });
    }
  };

  search = async key => {
    try {
      if (!key) {
        return this.setState(
          {
            offset: 0,
            ads: [],
            searched: false
          },
          async () => {
            await this.getAds();
          }
        );
      }

      this.setState(
        {
          ads: [],
          loading: true,
          offset: 0,
          searched: true
        },
        async () => {
          const { limit, offset } = this.state;
          const query = qs.stringify({
            status: "active",
            shop: "buy",
            sort: "-createdAt",
            limit,
            offset
          });
          const url = `http://buluruz.biz/api/ads/search/${key}?${query}`;
          const response = await fetch(url, {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          });
          const ads = await response.json();
          await this.setState({
            ads: [...this.state.ads, ...ads],
            error: response.error || null,
            loading: false,
            refreshing: false,
            offset: this.state.offset + 10
          });
        }
      );
    } catch (error) {
      console.error(error);
      this.setState({ error, loading: false });
    }
  };

  loadMoreAds = async () => {
    try {
      if (this.state.searched) {
        return;
      }
      this.setState(
        {
          offset: this.state.offset + 10
        },
        async () => {
          await this.getAds();
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  renderListItem = item => {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigator.push(Router.getRoute("ad"))}
        style={styles.listItemContainer}
      >
        <Image
          style={styles.listItemUserImage}
          source={{
            uri: item.owner.photo
          }}
        />
        <Text style={styles.listItemContent}>
          {_.truncate(item.content, {
            length: 110
          })}
        </Text>
        <Image
          style={styles.listItemCategoryImage}
          source={{
            uri: item.categories[0].image
          }}
        />
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listItemContainer: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    backgroundColor: "white"
  },
  listItemUserImage: {
    width: 60,
    height: 60,
    borderWidth: 2,
    borderColor: "#eee"
  },
  listItemCategoryImage: {
    width: 40,
    height: 40,
    marginLeft: 5,
    alignSelf: "center"
  },
  listItemContent: {
    flex: 1,
    paddingLeft: 10,
    alignSelf: "center"
  },
  listItemSeperator: {
    height: 1,
    backgroundColor: "#ddd"
  }
});
