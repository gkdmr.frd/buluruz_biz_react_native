import * as _ from 'lodash';
import React from 'react';
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import AuthStore from '../stores/AuthStore';

export default class FavoritesScreen extends React.Component {
    static route = {
        navigationBar: {
          title: "Favorilerim",
          titleStyle: {
            fontWeight: "bold"
          },
          tintColor: "#FF3366"
        }
      };

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            ads: [],
        };
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ flex: 1, backgroundColor: '#e4e3eb' }}>
                    {this.renderList()}
                </View>
            </SafeAreaView >
        );
    }

    renderList() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )
        }

        return (<FlatList
            keyExtractor={(item, index) => item._id}
            ItemSeparatorComponent={() => <View
                style={styles.listItemSeperator}
            />}
            data={this.state.ads}
            renderItem={({ item }) => this.renderListItem(item)}
        />)
    }

    componentDidMount() {
        this.getFavorites();
    }

    getFavorites = async () => {
        try {
            const url = `http://buluruz.biz/api/users/me/favoriteAds`;
            await this.setState({ loading: true });
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': AuthStore.user['accessToken']
                }
            });
            const ads = await response.json();
            this.setState({
                ads: [...this.state.ads, ...ads],
                loading: false,
            });
        } catch (error) {
            console.error(error);
            this.setState({ error, loading: false });
        }
    }

    renderListItem(item) {
        return <View style={styles.listItemContainer}>
            <Image
                style={styles.listItemUserImage}
                source={{
                    uri: item.owner.photo
                }}
            />
            <Text style={styles.listItemContent}>{_.truncate(item.content, {
                length: 110
            })}</Text>
            <Image
                style={styles.listItemCategoryImage}
                source={{
                    uri: item.categories[0].image
                }}
            />
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    listItemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        backgroundColor: 'white'
    },
    listItemUserImage: {
        width: 60,
        height: 60,
        borderWidth: 2,
        borderColor: '#eee'
    },
    listItemCategoryImage: {
        width: 40,
        height: 40,
        marginLeft: 5,
        alignSelf: 'center'
    },
    listItemContent: {
        flex: 1,
        paddingLeft: 10,
        alignSelf: 'center'
    },
    listItemSeperator: {
        height: 1,
        backgroundColor: "#ddd"
    }
})