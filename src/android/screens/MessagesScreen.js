import * as _ from "lodash";
import moment from "moment";
import "moment/locale/tr";
import React from "react";
import { ActivityIndicator, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native";
import AuthStore from "../stores/AuthStore";
moment.locale("tr");


export default class MessagesScreen extends React.Component {

  static route = {
    navigationBar: {
      title: "Mesajlar",
      titleStyle: {
        fontWeight: "bold"
      },
      tintColor: "#FF3366"
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      conversations: [],
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    this.getConversations();
  }

  getConversations = async () => {
    try {
      const url = `http://buluruz.biz/api/users/me/conversations`;
      this.setState({ loading: true });
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-access-token": AuthStore.user["accessToken"]
        }
      });
      const conversations = await response.json();
      this.setState({
        conversations,
        error: response.error || null,
        loading: false,
        refreshing: false
      });
    } catch (error) {
      console.error(error);
      this.setState({ error, loading: false });
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={{ flex: 1, backgroundColor: "#e4e3eb" }}>
            {this.renderList()}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  renderList() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <FlatList
        keyExtractor={(item, index) => item._id}
        ItemSeparatorComponent={() => <View style={styles.listItemSeperator} />}
        data={this.state.conversations}
        renderItem={({ item }) => this.renderListItem(item)}
      />
    );
  }

  renderListItem(item) {
    const to = _.head(
      _.filter(item.users, u => u._id != AuthStore.user["data"]["_id"])
    );
    return (
      <View style={styles.listItemContainer}>
        <Image
          style={styles.listItemUserImage}
          source={{
            uri: to.photo
          }}
        />
        <View style={styles.listItemCenter}>
          <Text style={styles.listItemUserName}>{to.fullName}</Text>
          <Text style={styles.listItemContent}>
            {_.truncate(item.lastMessage.content, {
              length: 150
            })}
          </Text>
        </View>
        <Text style={styles.listItemMessageDate}>
          {moment(item.lastMessage.createdAt).fromNow()}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listItemContainer: {
    flex: 1,
    flexDirection: "row",
    padding: 10,
    backgroundColor: "white"
  },
  listItemCenter: {
    flex: 1,
    flexDirection: "column",
    padding: 10,
    backgroundColor: "white"
  },
  listItemUserImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 2,
    borderColor: "#DDD"
  },
  listItemCategoryImage: {
    width: 30,
    height: 30
  },
  listItemContent: {
    flex: 1,
    color: "#666"
  },
  listItemUserName: {
    flex: 1,
    fontWeight: "bold",
    fontSize: 16
  },
  listItemMessageDate: {
    width: 50,
    alignSelf: "center",
    color: "#666",
    textAlign: "right",
    marginRight: 5,
    fontSize: 11
  },
  listItemSeperator: {
    height: 1,
    backgroundColor: "#ddd"
  }
});
