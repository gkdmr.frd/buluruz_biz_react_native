import {
    createRouter,
    StackNavigation,
    TabNavigation,
    TabNavigationItem as TabItem
  } from "@expo/ex-navigation";
  import React from "react";
  import { StyleSheet, Text, View } from "react-native";
  import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
  import AdScreen from "./ios/screens/AdScreen";
  import CreateAdScreen from "./ios/screens/CreateAdScreen";
  import FavoritesScreen from "./ios/screens/FavoritesScreen";
  import HomeScreen from "./ios/screens/HomeScreen";
  import IntroScreen from "./ios/screens/IntroScreen";
  import MessagesScreen from "./ios/screens/MessagesScreen";
  import MyAdsScreen from "./ios/screens/MyAdsScreen";
  import NotificationsScreen from "./ios/screens/NotificationsScreen";
  import PrivacyScreen from "./ios/screens/PrivacyScreen";
  import SettingsScreen from "./ios/screens/SettingsScreen";
  
  export const Router = createRouter(() => ({
    intro: () => IntroScreen,
    tabs: () => TabScreen,
    home: () => HomeScreen,
    ad: () => AdScreen,
    messages: () => MessagesScreen,
    messages: () => MessagesScreen,
    create_ad: () => CreateAdScreen,
    notifications: () => NotificationsScreen,
    settings: () => SettingsScreen,
    privacy: () => PrivacyScreen,
    favorites: () => FavoritesScreen,
    my_ads: () => MyAdsScreen
  }));
  
  class TabScreen extends React.Component {
    static route = {
      navigationBar: {
        visible: false
      }
    };
  
    render() {
      return (
        <View style={styles.container}>
          <TabNavigation id="main" navigatorUID="main" initialTab="home">
            <TabItem
              id="home"
              title="Anasayfa"
              renderTitle={(isSelected, title) => {
                return (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: "bold",
                      color: isSelected ? "#333" : "#949191"
                    }}
                  >
                    {title}
                  </Text>
                );
              }}
              renderIcon={isSelected => (
                <MaterialCommunityIcons
                  name={"home"}
                  size={26}
                  style={{ color: isSelected ? "#333" : "#949191" }}
                />
              )}
            >
              <StackNavigation
                id="home"
                navigatorUID="home"
                initialRoute={Router.getRoute("home")}
              />
            </TabItem>
  
            <TabItem
              id="messages"
              title="Mesajlar"
              renderTitle={(isSelected, title) => {
                return (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: "bold",
                      color: isSelected ? "#333" : "#949191"
                    }}
                  >
                    {title}
                  </Text>
                );
              }}
              renderIcon={isSelected => (
                <MaterialCommunityIcons
                  name={"email"}
                  size={26}
                  style={{ color: isSelected ? "#333" : "#949191" }}
                />
              )}
            >
              <StackNavigation
                id="posts"
                initialRoute={Router.getRoute("messages")}
              />
            </TabItem>
  
            <TabItem
              id="create_ad"
              renderIcon={isSelected => (
                <MaterialCommunityIcons
                  name={"plus-circle"}
                  size={56}
                  style={{ color: "#FF3366" }}
                />
              )}
            >
              <StackNavigation
                id="profile"
                initialRoute={Router.getRoute("create_ad")}
              />
            </TabItem>
            <TabItem
              id="notifications"
              title="Bildirimler"
              renderTitle={(isSelected, title) => {
                return (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: "bold",
                      color: isSelected ? "#333" : "#949191"
                    }}
                  >
                    {title}
                  </Text>
                );
              }}
              renderIcon={isSelected => (
                <MaterialCommunityIcons
                  name={"bell"}
                  size={26}
                  style={{ color: isSelected ? "#333" : "#949191" }}
                />
              )}
            >
              <StackNavigation
                id="profile"
                initialRoute={Router.getRoute("notifications")}
              />
            </TabItem>
            <TabItem
              id="settings"
              title="Daha Fazla"
              renderTitle={(isSelected, title) => {
                return (
                  <Text
                    style={{
                      fontSize: 11,
                      fontWeight: "bold",
                      color: isSelected ? "#333" : "#949191"
                    }}
                  >
                    {title}
                  </Text>
                );
              }}
              renderIcon={isSelected => (
                <MaterialCommunityIcons
                  name={"dots-horizontal"}
                  size={26}
                  style={{ color: isSelected ? "#333" : "#949191" }}
                />
              )}
            >
              <StackNavigation
                id="profile"
                initialRoute={Router.getRoute("settings")}
              />
            </TabItem>
          </TabNavigation>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fafafa',
    },
    selectedTab: {
      color: "#333"
    }
  });
  