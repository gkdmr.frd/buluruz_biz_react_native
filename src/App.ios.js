import { NavigationProvider, StackNavigation } from "@expo/ex-navigation";
import Instabug from "instabug-reactnative";
import React from "react";
import { StatusBar } from "react-native";
import AuthStore from "./ios/stores/AuthStore";
import { Router } from "./Router";


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      loading: true
    };
    Instabug.isRunningLive(function(isLive) {
      if (isLive) {
        Instabug.startWithToken(
          "a366a01607c69611958ad042e84e1926",
          Instabug.invocationEvent.none
        );
      } else {
        Instabug.startWithToken(
          "a45fff05006b932d548fc23ead93e20f",
          Instabug.invocationEvent.screenshot
        );
      }
      Instabug.setLocale(Instabug.locale.turkish);
    });
  }

  componentWillMount() {
    this.checkLogin();
  }

  render() {
    return (
      <NavigationProvider router={Router}>
        <StatusBar />
        {!this.state.loading &&
        <StackNavigation
          id="master"
          initialRoute={Router.getRoute(this.state.user ? "tabs" : "intro")}
        />}
      </NavigationProvider>
    );
  }

  checkLogin = async () => {
    const user = await AuthStore.getUser();
    this.setState({ user, loading: false });
  };
}
