import { observer } from "mobx-react";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Swiper from "react-native-swiper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { Router } from "../../Router";
import AuthStore from "../stores/AuthStore";


@observer
export default class IntroScreen extends React.Component {

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#e4e3eb" }}>
        <Swiper>
          <View style={styles.slide}>
            <Image
              style={styles.introImage}
              source={require("../../../assets/images/login_home.png")}
            />
            <Text style={styles.introText}>
              Aradığın evi anlat, emlakçılar sana fiyat versin!
            </Text>
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.introImage}
              source={require("../../../assets/images/login_job.png")}
            />
            <Text style={styles.introText}>
              Aradığın işi anlat, firmalar iş versin!
            </Text>
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.introImage}
              source={require("../../../assets/images/login_car.png")}
            />
            <Text style={styles.introText}>
              İstediğin arabayı anlat, galericiler sana fiyat versin!
            </Text>
          </View>
        </Swiper>
        <View style={{ paddingBottom: 20, paddingLeft: 12, paddingRight: 12 }}>
          <TouchableOpacity
            style={{
              backgroundColor: "#3e64a7",
              padding: 10,
              paddingLeft: 20,
              paddingRight: 20,
              marginRight: 10,
              marginLeft: 10,
              borderRadius: 6,
              marginBottom: 5,
              flexDirection: "row"
            }}
            onPress={() => this.loginWithFacebook()}
          >
            <MaterialCommunityIcons
              name={"facebook-box"}
              size={26}
              style={{ color: "white" }}
            />
            <Text
              style={{
                textAlign: "center",
                flex: 1,
                color: "#fff",
                fontSize: 18
              }}
            >
              Facebook ile devam et
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "rgb(220, 72, 59)",
              padding: 10,
              paddingLeft: 20,
              paddingRight: 20,
              marginRight: 10,
              marginLeft: 10,
              borderRadius: 6,
              marginBottom: 5,
              flexDirection: "row"
            }}
            onPress={() => this.loginWithGoogle()}
          >
            <MaterialCommunityIcons
              name={"google"}
              size={26}
              style={{ color: "white" }}
            />
            <Text
              style={{
                textAlign: "center",
                flex: 1,
                color: "#fff",
                fontSize: 18
              }}
            >
              Google ile devam et
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  loginWithFacebook = async () => {
    try {
      await AuthStore.loginWithFacebook();
      this.props.navigator.replace(Router.getRoute("tabs"));
    } catch (error) {
      console.log(error);
    }
  };

  loginWithGoogle = async () => {
    try {
      await AuthStore.loginWithGoogle();
      this.props.navigator.replace(Router.getRoute("tabs"));
    } catch (error) {
      console.log(error);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10
  },
  introText: {
    color: "#333",
    fontSize: 26,
    padding: 10,
    textAlign: "center"
  },
  introImage: {
    width: 180,
    height: 180
  },
  buttonText: { fontSize: 50, color: "#007aff" }
});
