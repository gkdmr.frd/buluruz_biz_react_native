import React from "react";
import { Alert, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";

export default class CreateAdScreen extends React.Component {
  static route = {
    navigationBar: {
      title: "Size ne lazım?",
      titleStyle: {
        fontWeight: "bold"
      },
      tintColor: "#FF3366"
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      content: null
    };
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View
            style={{
              flex: 1,
              paddingLeft: 15,
              paddingRight: 15,
              paddingTop: 20,
              backgroundColor: "#e4e3eb"
            }}
          >
            <Text style={styles.exampleText}>
              Lütfen ihtiyacınızı detaylıca açıklayın. İletişim bilgilerinizi
              ilana yazdığınızda daha hızlı cevap alabilirsiniz.
            </Text>
            <TextInput
              style={{
                borderColor: "#ccc",
                backgroundColor: "white",
                marginTop: 15,
                borderWidth: 3,
                padding: 10
              }}
              multiline={true}
              autoFocus
              onChangeText={content => this.setState({ content })}
              placeholder={`Örnek: İstanbul Avcılar'da 2+1 kiralık ev arıyorum. Kira 1500-2000 tl olsun. Metrobüse yakın olsun. 0 542 599 76 73 numaralı telefondan bana ulaşabilirsiniz.`}
              maxLength={800}
            />
            <TouchableOpacity
              style={{
                backgroundColor: "#FF3366",
                padding: 12,
                paddingLeft: 20,
                paddingRight: 20,
                borderRadius: 3,
                marginBottom: 10,
                marginTop: 15,
                flexDirection: "row"
              }}
              onPress={() => this.createAd()}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  flex: 1,
                  color: "#fff",
                  fontSize: 16
                }}
              >
                Yayınla
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  createAd = async () => {
    try {
      if (!this.state.content) {
        return Alert.alert(
          "Talep Oluştur",
          "Lütfen talebinize bi açıklama girin!",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: true }
        );
      }

      await fetch(`http://buluruz.biz/api/ads/create`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ content: this.state.content, shop: "buy" })
      });

      Alert.alert(
        "Talep Oluştur",
        "Talebiniz başarıyla oluşturuldu. Onaylandıktan sonra yayınlanacaktır.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: true }
      );
    } catch (error) {
      console.log(error);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  example: {
    fontWeight: "bold"
  },
  exampleText: {
    color: "#666",
    textAlign: "center",
    fontSize: 15
  }
});
