import React from "react";
import { Image, SafeAreaView, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AuthStore from "../stores/AuthStore";

export default class AdScreen extends React.Component {
  static route = {
    navigationBar: {
      title: "Talep",
      titleStyle: {
        fontWeight: "bold"
      },
      tintColor: "#FF3366",
      renderRight: () => (
        <TouchableOpacity
          style={{ alignSelf: "center", marginRight: 15, marginTop: 7 }}
        >
          <MaterialCommunityIcons
            name={"star-outline"}
            size={26}
            style={{ color: "#FF3366" }}
          />
        </TouchableOpacity>
      )
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      content: null
    };
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={{ flex: 1, backgroundColor: "#e4e3eb" }}>
            <Image
              style={styles.userPhoto}
              source={{
                uri: AuthStore.user["data"]["photo"]
              }}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  userPhoto: {
    width: 120,
    height: 120,
    alignSelf: "center",
    marginTop: 25,
    borderRadius: 60,
    borderWidth: 2,
    borderColor: "#ccc"
  }
});
