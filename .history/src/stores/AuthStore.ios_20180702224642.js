import { observable, action } from 'mobx';
import { AsyncStorage } from 'react-native';

class AuthStore {
    @observable user = null;

    @action async loginWithFacebook() {
        try {
            // const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1736733326631094', {
            //     permissions: ['email'],
            // });
            // if (type === 'success') {
            //     const userInfo = await fetch(`http://buluruz.biz/api/users/loginWithFacebook`, {
            //         method: 'POST',
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'application/json',
            //         },
            //         body: JSON.stringify({ facebookToken: token })
            //     });
            //     const userInfoJSON = await userInfo.json();
            //     await AsyncStorage.setItem('@Auth:user', JSON.stringify(userInfoJSON));
            //     this.user = userInfoJSON;
            // } else {
            //     throw new Error('Başarısız..');
            // }
        } catch (error) {
            throw error;
        }
    }

    @action async loginWithGoogle() {
        try {
            // const response = await Expo.Google.logInAsync({
            //     androidClientId: '815541547676-2rmgel9ctr7n0g82bclanmnj4qhv5a1u.apps.googleusercontent.com',
            //     iosClientId: '815541547676-rh5gltf8c6npj22s0o48unk2q326o24o.apps.googleusercontent.com',
            //     scopes: ['email'],
            // });

            // if (response.type == 'success') {
            //     const userInfo = await fetch(`http://buluruz.biz/api/users/loginWithGoogle`, {
            //         method: 'POST',
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'application/json',
            //         }, body: JSON.stringify({ response })
            //     });
            //     const userInfoJSON = await userInfo.json();
            //     await AsyncStorage.setItem('@Auth:user', JSON.stringify(userInfoJSON));
            //     this.user = userInfoJSON;
            // }
        } catch (error) {
            throw error;
        }
    }

    @action async getUser() {
        try {
            this.user = JSON.parse(await AsyncStorage.getItem('@Auth:user'));
            return this.user;
        } catch (error) {
            throw error;
        }
    }

    @action async logout() {
        try {
            await AsyncStorage.removeItem('@Auth:user');
            this.user = null;
        } catch (error) {
            throw error;
        }
    }
}

export default new AuthStore()