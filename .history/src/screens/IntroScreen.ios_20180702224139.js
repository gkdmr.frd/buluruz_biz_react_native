import React from 'react';
import { Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import Swiper from 'react-native-swiper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { observer } from 'mobx-react';
import AuthStore from '../stores/AuthStore';

@observer
export default class IntroScreen extends React.Component {
    static navigationOptions = {
        title: 'Giriş',
        headerStyle: {
            backgroundColor: '#fff'
        },
        headerTintColor: '#FF3366',
        headerTitleStyle: {
            flex: 1,
            textAlign: "center",
        },
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, backgroundColor: '#e4e3eb' }}>

                <Swiper>
                    <View style={styles.slide}>
                        <Image
                            style={styles.introImage}
                            source={require('../../assets/images/login_home.png')}
                        />
                        <Text style={styles.introText}>Aradığın evi anlat, emlakçılar sana fiyat versin!</Text>
                    </View>
                    <View style={styles.slide}>
                        <Image
                            style={styles.introImage}
                            source={require('../../assets/images/login_job.png')}
                        />
                        <Text style={styles.introText}>Aradığın işi anlat, firmalar iş versin!</Text>
                    </View>
                    <View style={styles.slide}>
                        <Image
                            style={styles.introImage}
                            source={require('../../assets/images/login_car.png')}
                        />
                        <Text style={styles.introText}>İstediğin arabayı anlat, galericiler sana fiyat versin!</Text>
                    </View>
                </Swiper>
                <View style={{ paddingBottom: 20, paddingLeft:15, paddingRight:15 }}>
                    <TouchableOpacity style={{
                        backgroundColor: '#3e64a7',
                        padding: 10,
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginRight: 10,
                        marginLeft: 10,
                        borderRadius: 6,
                        marginBottom: 5,
                        flexDirection: 'row'
                    }} onPress={() => this.loginWithFacebook(navigate)}>
                        <MaterialCommunityIcons
                            name={'facebook-box'}
                            size={26}
                            style={{ color: 'white' }}
                        />
                        <Text style={{ textAlign: 'center', flex: 1, color: '#fff', fontSize: 18 }}>Facebook ile devam et</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: 'rgb(220, 72, 59)',
                        padding: 10,
                        paddingLeft: 20,
                        paddingRight: 20,
                        marginRight: 10,
                        marginLeft: 10,
                        borderRadius: 6,
                        marginBottom: 5,
                        flexDirection: 'row'
                    }} onPress={() => this.loginWithGoogle(navigate)}>
                        <MaterialCommunityIcons
                            name={'google'}
                            size={26}
                            style={{ color: 'white' }}
                        />
                        <Text style={{ textAlign: 'center', flex: 1, color: '#fff', fontSize: 18 }}>Google ile devam et</Text>
                    </TouchableOpacity>
                </View>
            </View>

        );
    }

    async loginWithFacebook(navigate) {
        try {
            await AuthStore.loginWithFacebook();
            navigate('Tabs');
        } catch (error) {
           console.log(error);
        }
    }

    async loginWithGoogle(navigate) {
        try {
            await AuthStore.loginWithGoogle();
            navigate('Tabs');
        } catch (error) {
            console.log(error);
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    introText: {
        color: '#333',
        fontSize: 26,
        padding: 10,
        textAlign: 'center'
    },
    introImage: {
        width: 180,
        height: 180
    },
    buttonText: { fontSize: 50, color: '#007aff', }
})