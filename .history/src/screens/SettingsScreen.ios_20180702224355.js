import React from 'react';
import { Text, View, SafeAreaView, Alert, TouchableOpacity, Image, FlatList, StyleSheet, ScrollView } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AuthStore from '../stores/AuthStore';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Ayarlar',
    headerStyle: {
      backgroundColor: '#fff'
    },
    headerTintColor: '#FF3366',
    headerTitleStyle: {
      flex: 1,
      textAlign: "center",
    },
    headerBackTitle: 'Geri',
  };

  constructor(props) {
    super(props)
    this.state = {
      menuItems1: [
        {
          icon: 'view-list',
          iconColor: '#FF3366',
          title: 'Taleplerim',
          screen: ''
        },
        {
          icon: 'star-circle',
          iconColor: '#FF3366',
          title: 'Favorilerim',
          screen: ''
        },
        {
          icon: 'share',
          iconColor: '#FF3366',
          title: 'Arkadaşını Davet Et',
          screen: ''
        },
        {
          icon: 'email-alert',
          iconColor: '#FF3366',
          title: 'Dilek Şikayet',
          screen: ''
        },
        {
          icon: 'file-document-box',
          iconColor: '#FF3366',
          title: 'Gizlilik Sözleşmesi',
          screen: 'PrivacyScreen'
        }
      ]
    };
  }

  render(){
    return null;
  }

  // render() {
  //   return (
  //     <SafeAreaView style={styles.container}>
  //       <ScrollView>
  //         <View style={{ flex: 1, backgroundColor: '#e4e3eb' }}>
  //           <Image style={styles.profilePhoto}
  //             source={{
  //               uri: AuthStore.user['data']['photo']
  //             }}
  //           />
  //           <Text style={styles.profileFullname}>{AuthStore.user['data']['fullName']}</Text>
  //           <View>
  //             <FlatList
  //               style={styles.menuItems}
  //               keyExtractor={(item, index) => index.toString()}
  //               ItemSeparatorComponent={() => <View
  //                 style={styles.listItemSeperator}
  //               />}
  //               data={this.state.menuItems1}
  //               renderItem={({ item }) => this.renderMenuItem(item)}
  //             />
  //           </View>

  //           <View style={styles.appInfo}>
  //             <Image style={styles.appIcon}
  //               source={require('../../assets/images/icon.png')}
  //             />
  //             <Text style={styles.appVersion}>{'Ver.' + Expo.Constants.manifest.version}</Text>
  //             <Text style={styles.copyright}>{'@2018 buluruz.biz'}</Text>
  //           </View>

  //           <TouchableOpacity style={{ margin: 20 }} onPress={() => this.logout()}>
  //             <View style={styles.menuItemContainer}>
  //               <MaterialCommunityIcons
  //                 name={'account'}
  //                 size={28}
  //                 style={{ color: '#FF3366' }}
  //               />
  //               <Text style={styles.menuItemTitle}>Çıkış Yap</Text>
  //               <MaterialCommunityIcons
  //                 name={'chevron-right'}
  //                 size={21}
  //                 style={{ color: '#999', alignSelf: 'center' }}
  //               />
  //             </View>
  //           </TouchableOpacity>
  //         </View>
  //       </ScrollView>
  //     </SafeAreaView>
  //   );
  // }

  // renderMenuItem = (item) => {
  //   return <TouchableOpacity onPress={this.openScreen.bind(this, item.screen)} style={styles.menuItemContainer}>
  //     <MaterialCommunityIcons
  //       name={item.icon}
  //       size={28}
  //       style={{ color: item.iconColor, alignSelf: 'center' }}
  //     />
  //     <Text style={styles.menuItemTitle}>{item.title}</Text>
  //     <MaterialCommunityIcons
  //       name={'chevron-right'}
  //       size={21}
  //       style={{ color: '#999', alignSelf: 'center' }}
  //     />
  //   </TouchableOpacity>;
  // }

  // logout = async () => {
  //   try {
  //     Alert.alert(
  //       'Çıkış Yap',
  //       'Gerçekten çıkış yapmak istiyor musunuz?',
  //       [
  //         { text: 'Vazgeç', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
  //         {
  //           text: 'Evet', onPress: async () => {
  //             await AuthStore.logout();
  //             this.props.navigation.navigate('Intro');
  //           }
  //         },
  //       ],
  //       { cancelable: true }
  //     )
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // openScreen = (screen) => {
  //   this.props.navigation.navigate(screen);
  // }

}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  profilePhoto: {
    width: 120,
    height: 120,
    alignSelf: 'center',
    marginTop: 25,
    borderRadius: 60,
    borderWidth: 2,
    borderColor: '#ccc'
  },
  profileFullname:
  {
    fontSize: 21,
    textAlign: 'center',
    margin: 20,
    fontWeight: 'bold'
  },
  menuItems: {
    margin: 20,
    marginTop: 0
  },
  menuItemContainer: {
    backgroundColor: '#fafafa',
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row'
  },
  menuItemTitle: {
    alignSelf: 'center',
    paddingLeft: 15,
    fontSize: 16,
    flex: 1
  },
  listItemSeperator: {
    height: 1,
    backgroundColor: "#ddd"
  },
  appInfo: {
    margin: 20
  },
  appIcon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    borderRadius: 25
  },
  appVersion:
  {
    fontSize: 13,
    textAlign: 'center',
    margin: 10,
    fontWeight: 'bold'
  },
  copyright:
  {
    fontSize: 13,
    textAlign: 'center',
  },
})