import React from 'react';
import { Text, View, ActivityIndicator, SafeAreaView, Image, FlatList, StyleSheet, ScrollView } from 'react-native';
import moment from 'moment';
import 'moment/locale/tr';
moment.locale('tr');

import AuthStore from '../stores/AuthStore';

export default class NotificationsScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Bildirimler',
            headerStyle: {
                backgroundColor: '#fff'
            },
            headerTintColor: '#FF3366',
            headerTitleStyle: {
                flex: 1,
                textAlign: "center",
            }
        }
    };

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            notifications: [],
            error: null,
            refreshing: false,
        };
    }

    componentDidMount() {
        this.getConversations();
    }

    getConversations = async () => {
        try {
            const url = `http://buluruz.biz/api/users/me/notifications`;
            this.setState({ loading: true });
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': AuthStore.user['accessToken']
                }
            });
            const notifications = await response.json();
            this.setState({
                notifications,
                error: response.error || null,
                loading: false,
                refreshing: false
            });
        } catch (error) {
            console.error(error);
            this.setState({ error, loading: false });
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={{ flex: 1, backgroundColor: '#e4e3eb' }}>
                        {this.renderList()}
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }

    renderList() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>
                    <ActivityIndicator />
                </View>
            )
        }
        return (<FlatList
            keyExtractor={(item, index) => item._id}
            ItemSeparatorComponent={() => <View
                style={styles.listItemSeperator}
            />}
            data={this.state.notifications}
            renderItem={({ item }) => this.renderListItem(item)}
        />)
    }

    renderListItem(item) {
        return <View style={styles.listItemContainer}>
            {this.renderListItemImage(item)}
            <View style={styles.listItemCenter}>
                {this.renderListItemContent(item)}
            </View>
            <Text style={styles.listItemMessageDate}>{moment(item.createdAt).fromNow()}</Text>
        </View>
    }

    renderListItemContent(item) {
        if (item.meta.type == 'adView') {
            return <Text style={styles.listItemUserName}>{item.from.fullName}<Text style={styles.listItemContent}>{` talebini görüntüledi.`}</Text></Text>
        }
        if (item.meta.type == 'favoriteAd') {
            return <Text style={styles.listItemUserName}>{item.from.fullName}<Text style={styles.listItemContent}>{` talebini favorilere ekledi.`}</Text></Text>
        }
        if (item.meta.type == 'adPublish') {
            return <Text style={styles.listItemUserName}>{item.content}</Text>
        }
        return null;
    }

    renderListItemImage(item) {
        if (item.meta.type == 'favoriteAd' || item.meta.type == 'adView') {
            return <Image style={styles.listItemUserImage}
                source={{
                    uri: item.from.photo
                }}
            />;
        }
        return <Image style={styles.listItemUserImage}
            source={require('../assets/images/icon.png')}
        />;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    listItemContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        backgroundColor: 'white'
    },
    listItemCenter: {
        flex: 1,
        flexDirection: 'column',
        padding: 10,
        backgroundColor: 'white'
    },
    listItemUserImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: '#DDD'
    },
    listItemCategoryImage: {
        width: 30,
        height: 30
    },
    listItemContent: {
        flex: 1,
        color: '#666',
        fontSize: 14,
        fontWeight:'normal'
    },
    listItemUserName: {
        flex: 1,
        fontWeight: 'bold',
        fontSize: 14,
    },
    listItemMessageDate: {
        width: 50,
        alignSelf: 'center',
        color: '#666',
        textAlign: 'right',
        marginRight: 5,
        fontSize:11,
    },
    listItemSeperator: {
        height: 1,
        backgroundColor: "#ddd"
    }
})