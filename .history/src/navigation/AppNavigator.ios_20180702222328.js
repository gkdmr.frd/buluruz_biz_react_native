import React from 'react';
import { View, StyleSheet, ActivityIndicator, StatusBar } from 'react-native';

import {
    createBottomTabNavigator,
    createStackNavigator,
    createSwitchNavigator
} from 'react-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import AuthStore from '../stores/AuthStore.ios';

import HomeScreen from '../screens/HomeScreen';
import IntroScreen from '../screens/IntroScreen';
import MessagesScreen from '../screens/MessagesScreen';
import CreateAdScreen from '../screens/CreateAdScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import PrivacyScreen from '../screens/PrivacyScreen';

const HomeStack = createStackNavigator({
    HomeScreen,
});

const MessagesStack = createStackNavigator({
    MessagesScreen,
});

const CreateAdStack = createStackNavigator({
    CreateAdScreen,
});

const NotificationsStack = createStackNavigator({
    NotificationsScreen,
});

const SettingsStack = createStackNavigator({
    SettingsScreen,
    PrivacyScreen
});

const TabsStack = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            path: '/home',
            navigationOptions: {
                tabBarLabel: 'Anasayfa',
                tabBarIcon: ({ tintColor, focused }) => (
                    <MaterialCommunityIcons
                        name={'home'}
                        size={26}
                        style={{ color: tintColor }}
                    />
                )
            },
        },
        Messages: {
            screen: MessagesStack,
            path: '/messages',
            navigationOptions: {
                tabBarLabel: 'Mesajlar',
                tabBarIcon: ({ tintColor, focused }) => (
                    <MaterialCommunityIcons
                        name={'email'}
                        size={26}
                        style={{ color: tintColor }}
                    />
                )
            },
        },
        CreateAd: {
            screen: CreateAdStack,
            path: '/create_ad',
            navigationOptions: {
                tabBarLabel: 'Yayınla',
                tabBarIcon: ({ tintColor, focused }) => (
                    <MaterialCommunityIcons
                        name={'plus-circle'}
                        size={26}
                        style={{ color: '#FF3366' }}
                    />
                )
            },
        },
        Notifications: {
            screen: NotificationsStack,
            path: '/notifications',
            navigationOptions: {
                tabBarLabel: 'Bildirimler',
                tabBarIcon: ({ tintColor, focused }) => (
                    <MaterialCommunityIcons
                        name={'bell'}
                        size={26}
                        style={{ color: tintColor }}
                    />
                )
            },
        },
        Settings: {
            screen: SettingsStack,
            path: '/settings',
            navigationOptions: {
                tabBarLabel: 'Daha Fazla',
                tabBarIcon: ({ tintColor, focused }) => (
                    <MaterialCommunityIcons
                        name={'dots-horizontal'}
                        size={26}
                        style={{ color: tintColor }}
                    />
                )
            },
        },
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            showLabel: true,
            showIcon: true,
            labelStyle: {
                fontWeight: 'bold'
            },
            activeTintColor: '#333',
            inactiveTintColor: '#949191',
            style: {
                backgroundColor: '#ffffff'
            }
        },
    }
);

class AuthLoadingScreen extends React.Component {
    constructor() {
        super();
        this.checkSession();
    }
    checkSession = async () => {
        const user = await AuthStore.getUser();
        this.props.navigation.navigate(user ? 'Tabs' : 'Intro');
    };

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const RootStack = createSwitchNavigator({
    AuthLoading: AuthLoadingScreen,
    Intro: IntroScreen,
    Tabs: { screen: TabsStack },
}, {
        initialRouteName: 'AuthLoading'
    });

export default RootStack;